## [1.1.5](https://gitlab.com/dreamer-labs/iac/ansible_role_galera/compare/v1.1.4...v1.1.5) (2021-01-22)


### Bug Fixes

* Fix linting issues with updated ansible lint ([8100014](https://gitlab.com/dreamer-labs/iac/ansible_role_galera/commit/8100014))

## [1.1.4](https://gitlab.com/dreamer-labs/iac/ansible_role_galera/compare/v1.1.3...v1.1.4) (2021-01-21)


### Bug Fixes

* molecule tests + add support for required vars ([2d30fbc](https://gitlab.com/dreamer-labs/iac/ansible_role_galera/commit/2d30fbc))

## [1.1.3](https://gitlab.com/dreamer-labs/iac/ansible_role_galera/compare/v1.1.2...v1.1.3) (2020-08-04)


### Bug Fixes

* Update the IUS repo to reflect upstream change ([670f483](https://gitlab.com/dreamer-labs/iac/ansible_role_galera/commit/670f483))

## [1.1.2](https://gitlab.com/dreamer-labs/iac/ansible_role_galera/compare/v1.1.1...v1.1.2) (2020-03-24)


### Bug Fixes

* fixed clustercheck script ([e820167](https://gitlab.com/dreamer-labs/iac/ansible_role_galera/commit/e820167))

## [1.1.1](https://gitlab.com/dreamer-labs/iac/ansible_role_galera/compare/v1.1.0...v1.1.1) (2019-11-25)


### Bug Fixes

* Fix variable handling and deprecation warnings ([b276970](https://gitlab.com/dreamer-labs/iac/ansible_role_galera/commit/b276970)), closes [#6](https://gitlab.com/dreamer-labs/iac/ansible_role_galera/issues/6)

# [1.1.0](https://gitlab.com/dreamer-labs/maniac/ansible_role_galera/compare/v1.0.0...v1.1.0) (2019-11-20)


### Features

* Add image creation in ci testing ([5d9f21d](https://gitlab.com/dreamer-labs/maniac/ansible_role_galera/commit/5d9f21d))

# 1.0.0 (2019-11-18)


### Bug Fixes

* Fix package dictionary in container test ([88b0ebd](https://gitlab.com/dreamer-labs/maniac/ansible_role_galera/commit/88b0ebd))
* Rename one handler to comply to naming ([af229f0](https://gitlab.com/dreamer-labs/maniac/ansible_role_galera/commit/af229f0))
