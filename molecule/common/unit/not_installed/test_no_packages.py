import os
import pytest

import yaml

import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('galera_nodes')


@pytest.fixture
def role_defaults():
    pth = os.path.realpath(os.path.join(__file__,
                                        *(['..'] * 5),
                                        'defaults', 'main.yml'))
    with open(pth) as ymlfile:
        role_defaults = yaml.safe_load(ymlfile)

    return role_defaults


def test_absent_packages(host, role_defaults):
    installed = []
    for pkg_name in role_defaults['galera_packages']['yum']:
        if host.package(pkg_name).is_installed:
            installed.append(pkg_name)

    assert len(installed) == 0, (
        'These packages should not be installed {installed}'
    )
