import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('galera_nodes')


def test_connections(host):
    assert host.socket("tcp://3306").is_listening


def test_custer_state_uuid(sql_query):
    cluster_state = []

    for hostname in testinfra_hosts:
        host = testinfra.utils.ansible_runner.AnsibleRunner(
            os.environ['MOLECULE_INVENTORY_FILE']).get_host(hostname)
        cluster_state.append(
            sql_query(host,
                      "SHOW GLOBAL STATUS LIKE 'wsrep_cluster_state_uuid';"))

    assert len(set([x[1] for x in cluster_state])) == 1


def test_cluster_count(sql_query_host):

    result = sql_query_host("SHOW GLOBAL STATUS LIKE 'wsrep_cluster_size';")

    assert len(testinfra_hosts) == int(result[1])


def test_cluster_status(sql_query_host):

    result = sql_query_host("SHOW GLOBAL STATUS LIKE 'wsrep_cluster_status';")

    assert result[1] == 'Primary'
