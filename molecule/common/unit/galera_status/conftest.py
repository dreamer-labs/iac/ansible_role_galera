import yaml

import pytest


SQL_CONF_VARS = ('galera_clustercheck_password',
                 'galera_clustercheck_user',
                 'galera_cluster_bind_ip',
                 'galera_cluster_ips',
                 'galera_ssl')


@pytest.fixture
def sql_config(config_path):
    with open(config_path) as ymlfile:
        values = yaml.safe_load(ymlfile)[0]['roles'][0]

    return {k: v for k, v in
            values.items()
            if k in SQL_CONF_VARS}


@pytest.fixture
def sql_query(sql_config):

    def query_command(host, sql_query, ssl=False, host_conn='localhost',
                      raw=False):

        sql_command = (
            'mysql -nNE --connect-timeout=1'
            f' --user={sql_config["galera_clustercheck_user"]}'
            f' --password={sql_config["galera_clustercheck_password"]}'
            f' --host={host_conn} --port=3306'
            f' -e "{sql_query}"'
        )

        if ssl:
            sql_command = sql_command + ' --ssl'

        result = host.run(sql_command)
        if raw:
            return result
        else:
            return query_data(result.stdout)

    return query_command


def query_data(query_result):
    return query_result.split('\n')[1:3]


@pytest.fixture
def sql_query_host(host, sql_query):

    def sql_query_wrapper(sql_query_str, ssl=False, host_conn='localhost',
                          raw=False):
        return sql_query(host, sql_query_str, ssl, host_conn, raw)

    return sql_query_wrapper
