import os
import testinfra.utils.ansible_runner

import pytest

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('galera_nodes')


@pytest.fixture
def host_with_openssl11(host):

    host.run('openssl11 ||'
             ' yum install -y epel-release; yum install -y openssl11;')

    return host


def test_raw_ssl_myslqd(host_with_openssl11):

    result = host_with_openssl11.run(
        'openssl11 s_client -starttls mysql localhost:3306').stdout

    assert 'Server public key is 4096 bit' in result


def test_raw_ssl_galera_repl(host_with_openssl11):

    result = host_with_openssl11.run(
        'openssl11 s_client -connect localhost:4567'
    ).stdout

    assert 'Server public key is 4096 bit' in result


def test_ssl_with_mysql(sql_query_host):

    result = sql_query_host("show status like 'Ssl_cipher';", ssl=True,
                            host_conn='127.0.0.1')

    assert result[0] == 'Ssl_cipher' and result[1] != ''


def test_ssl_required(sql_query_host):

    result = sql_query_host("show status like 'Ssl_cipher';", ssl=False,
                            host_conn='127.0.0.1', raw=True)

    assert 'Access denied for user' in result.stderr
