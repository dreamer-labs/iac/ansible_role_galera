import os
import testinfra.utils.ansible_runner

import pytest

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('galera_nodes')


@pytest.fixture
def host_with_openssl11(host):

    host.run('openssl11 ||'
             ' yum install -y epel-release; yum install -y openssl11;')

    return host


def test_no_ssl_myslqd(host_with_openssl11):

    result = host_with_openssl11.run(
        'openssl11 s_client -starttls mysql localhost:3306').stderr

    assert 'MySQL server does not support SSL.' in result


def test_no_ssl_galera_repl(host_with_openssl11):

    result = host_with_openssl11.run(
        'openssl11 s_client -connect localhost:4567'
    ).stdout

    assert 'Secure Renegotiation IS NOT supported' in result
