import os
import pytest


@pytest.fixture
def config_path():
    return os.path.join(os.path.dirname(__file__), '..', 'converge.yml')
